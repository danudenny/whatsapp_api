const message = (sequelize, DataTypes) => {
    const Message = sequelize.define('messages', {
        id: {
            primaryKey: true,
            allowNull: false,
            autoIncrement: true,
            type: DataTypes.INTEGER,
        },
        sessionId: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        receiver: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        message: {
            type: DataTypes.JSON,
            allowNull: true,
        },
        status: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
        },
        errorLog: {
            type: DataTypes.STRING,
            allowNull: true,
        },
    })
    return Message
}

export default message
