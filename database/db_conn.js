export const development = {
    dbName: process.env.DB_NAME,
    dbHost: process.env.DB_HOST,
    dbPass: process.env.DB_PASS,
    dbUser: process.env.DB_USER,
    dbPort: process.env.DB_PORT,
    dialect: 'mysql',
}
