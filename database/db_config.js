import { Sequelize } from 'sequelize'
import * as envConfigs from './db_conn.js'

import message from './models/message.js'

const env = process.env.NODE_ENV || 'development'
const config = envConfigs[env]
const db = {}

const seqConn = new Sequelize(config.dbName, config.dbUser, config.dbPass, {
    host: config.dbHost,
    dialect: config.dialect,
})

db.sequelize = seqConn
db.Sequelize = Sequelize

db.message = message(seqConn, Sequelize)

export default db
